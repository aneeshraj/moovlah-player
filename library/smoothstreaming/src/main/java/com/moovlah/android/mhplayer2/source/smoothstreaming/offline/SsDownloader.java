/*
 * Copyright (C) 2017 The Android Open Source Project
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.moovlah.android.mhplayer2.source.smoothstreaming.offline;

import static com.moovlah.android.mhplayer2.util.Assertions.checkNotNull;

import android.net.Uri;
import com.moovlah.android.mhplayer2.MediaItem;
import com.moovlah.android.mhplayer2.offline.SegmentDownloader;
import com.moovlah.android.mhplayer2.offline.StreamKey;
import com.moovlah.android.mhplayer2.source.smoothstreaming.manifest.SsManifest;
import com.moovlah.android.mhplayer2.source.smoothstreaming.manifest.SsManifestParser;
import com.moovlah.android.mhplayer2.upstream.DataSource;
import com.moovlah.android.mhplayer2.upstream.DataSpec;
import com.moovlah.android.mhplayer2.upstream.cache.CacheDataSource;
import com.moovlah.android.mhplayer2.util.Util;
import com.moovlah.android.mhplayer2.upstream.ParsingLoadable;
import com.moovlah.android.mhplayer2.util.Assertions;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.Executor;

/**
 * A downloader for SmoothStreaming streams.
 *
 * <p>Example usage:
 *
 * <pre>{@code
 * SimpleCache cache = new SimpleCache(downloadFolder, new NoOpCacheEvictor(), databaseProvider);
 * CacheDataSource.Factory cacheDataSourceFactory =
 *     new CacheDataSource.Factory()
 *         .setCache(cache)
 *         .setUpstreamDataSourceFactory(new DefaultHttpDataSourceFactory(userAgent));
 * // Create a downloader for the first track of the first stream element.
 * SsDownloader ssDownloader =
 *     new SsDownloader(
 *         new MediaItem.Builder()
 *             .setUri(manifestUri)
 *             .setStreamKeys(Collections.singletonList(new StreamKey(0, 0)))
 *             .build(),
 *         cacheDataSourceFactory);
 * // Perform the download.
 * ssDownloader.download(progressListener);
 * // Use the downloaded data for playback.
 * SsMediaSource mediaSource =
 *     new SsMediaSource.Factory(cacheDataSourceFactory).createMediaSource(mediaItem);
 * }</pre>
 */
public final class SsDownloader extends SegmentDownloader<SsManifest> {

  /**
   * @deprecated Use {@link #SsDownloader(MediaItem, CacheDataSource.Factory, Executor)} instead.
   */
  @SuppressWarnings("deprecation")
  @Deprecated
  public SsDownloader(
      Uri manifestUri, List<StreamKey> streamKeys, CacheDataSource.Factory cacheDataSourceFactory) {
    this(manifestUri, streamKeys, cacheDataSourceFactory, Runnable::run);
  }

  /**
   * Creates an instance.
   *
   * @param mediaItem The {@link MediaItem} to be downloaded.
   * @param cacheDataSourceFactory A {@link CacheDataSource.Factory} for the cache into which the
   *     download will be written.
   */
  public SsDownloader(MediaItem mediaItem, CacheDataSource.Factory cacheDataSourceFactory) {
    this(mediaItem, cacheDataSourceFactory, Runnable::run);
  }

  /**
   * @deprecated Use {@link #SsDownloader(MediaItem, CacheDataSource.Factory, Executor)} instead.
   */
  @Deprecated
  public SsDownloader(
      Uri manifestUri,
      List<StreamKey> streamKeys,
      CacheDataSource.Factory cacheDataSourceFactory,
      Executor executor) {
    this(
        new MediaItem.Builder().setUri(manifestUri).setStreamKeys(streamKeys).build(),
        cacheDataSourceFactory,
        executor);
  }

  /**
   * Creates an instance.
   *
   * @param mediaItem The {@link MediaItem} to be downloaded.
   * @param cacheDataSourceFactory A {@link CacheDataSource.Factory} for the cache into which the
   *     download will be written.
   * @param executor An {@link Executor} used to make requests for the media being downloaded.
   *     Providing an {@link Executor} that uses multiple threads will speed up the download by
   *     allowing parts of it to be executed in parallel.
   */
  public SsDownloader(
      MediaItem mediaItem, CacheDataSource.Factory cacheDataSourceFactory, Executor executor) {
    this(
        mediaItem
            .buildUpon()
            .setUri(
                Util.fixSmoothStreamingIsmManifestUri(
                    Assertions.checkNotNull(mediaItem.playbackProperties).uri))
            .build(),
        new SsManifestParser(),
        cacheDataSourceFactory,
        executor);
  }

  /**
   * Creates a new instance.
   *
   * @param mediaItem The {@link MediaItem} to be downloaded.
   * @param manifestParser A parser for SmoothStreaming manifests.
   * @param cacheDataSourceFactory A {@link CacheDataSource.Factory} for the cache into which the
   *     download will be written.
   * @param executor An {@link Executor} used to make requests for the media being downloaded.
   *     Providing an {@link Executor} that uses multiple threads will speed up the download by
   *     allowing parts of it to be executed in parallel.
   */
  public SsDownloader(
      MediaItem mediaItem,
      ParsingLoadable.Parser<SsManifest> manifestParser,
      CacheDataSource.Factory cacheDataSourceFactory,
      Executor executor) {
    super(mediaItem, manifestParser, cacheDataSourceFactory, executor);
  }

  @Override
  protected List<Segment> getSegments(
      DataSource dataSource, SsManifest manifest, boolean allowIncompleteList) {
    ArrayList<Segment> segments = new ArrayList<>();
    for (SsManifest.StreamElement streamElement : manifest.streamElements) {
      for (int i = 0; i < streamElement.formats.length; i++) {
        for (int j = 0; j < streamElement.chunkCount; j++) {
          segments.add(
              new Segment(
                  streamElement.getStartTimeUs(j),
                  new DataSpec(streamElement.buildRequestUri(i, j))));
        }
      }
    }
    return segments;
  }

}
